//
//  Constants.swift
//  Aclena
//
//  Created by Karthik Sakthivel on 21/10/17.
//  Copyright © 2017 Uberdoo. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Constants
{
    //App Constants
    static var baseURL = "http://35.178.27.62/Aclena/public"
    static var socketURL = "http://35.178.27.62:3000/"
    static var adminBaseURL = "http://104.131.74.144/uber_test/public/admin"
//    static var mapsKey = "AIzaSyCPjWxddhIse0IxvPd9izDAAYKTwxtZf5Y"
//    static var placesKey = "AIzaSyCPjWxddhIse0IxvPd9izDAAYKTwxtZf5Y"
//    static var directionsKey = "AIzaSyCx3EumWM8q51b6QwIhd6FWsBJkDKgPlwY"
    
//    static var mapsKey = "AIzaSyBXyK-3yvYz5zhnpIeUt6hWPChWZfqfyhg"
//    static var placesKey = "AIzaSyBXyK-3yvYz5zhnpIeUt6hWPChWZfqfyhg"
//    static var directionsKey = "AIzaSyBXyK-3yvYz5zhnpIeUt6hWPChWZfqfyhg"

    
    static var mapsKey = "AIzaSyAN3zOM6rFTuuzcF5k1_9RzAHXVfE16Iu8"
    static var placesKey = "AIzaSyAN3zOM6rFTuuzcF5k1_9RzAHXVfE16Iu8"
    static var directionsKey = "AIzaSyAN3zOM6rFTuuzcF5k1_9RzAHXVfE16Iu8"

    static var locations : [JSON] = []
    static var timeSlots : [JSON] = []

}
