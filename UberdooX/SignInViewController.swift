//
//  SignInViewController.swift
//  Aclena
//
//  Created by Karthik Sakthivel on 12/10/17.
//  Copyright © 2017 Uberdoo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SwiftSpinner
import Firebase
import FirebaseMessaging
import FBSDKLoginKit
import FBSDKCoreKit
import GoogleSignIn

class SignInViewController: UIViewController,UITextFieldDelegate, GIDSignInUIDelegate, GIDSignInDelegate {
   
    

    @IBOutlet weak var passwordFld: UITextField!
    @IBOutlet weak var UsernameFld: UITextField!
    var firstName : String!
    var lastName : String!
    var email : String!
    var image : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        

        
        passwordFld.delegate = self
        UsernameFld.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func validateForm() -> Bool{
        
        if let text = UsernameFld.text, !text.isEmpty
        {
        }
        else{
            showAlert(title: "Validation Failed",msg: "Invalid Email")
            return false
        }
        if let text = passwordFld.text, !text.isEmpty
        {
            return true
        }
        else{
            showAlert(title: "Validation Failed",msg: "Invalid Password")
            return false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        switch textField
        {
        case UsernameFld:
            passwordFld.becomeFirstResponder()
            break
        default:
            textField.resignFirstResponder()
        }
        return true
    }


    func socialLogin(socialType: String!, socialToken : String!){
        let params: Parameters = [
            "firstname": firstName,
            "lastname" : lastName,
            "social_type": socialType,
            "socialtoken":socialToken,
            "email": email
        ]
        SwiftSpinner.show("Logging in...")
        let url = "\(Constants.baseURL)/api/sociallogin"
        Alamofire.request(url,method: .post,parameters:params).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                SwiftSpinner.hide()
                if let json = response.result.value {
                    print("LOGIN JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    if(jsonResponse["error"].stringValue == "true")
                    {
                        let errorMessage = jsonResponse["error_message"].stringValue
                        self.showAlert(title: "Login Failed",msg: errorMessage)
                    }
                    else{
                        let access_token = "Bearer ".appending(jsonResponse["access_token"].stringValue)
                        
                        let userid = jsonResponse["user_id"].stringValue
                        
                        UserDefaults.standard.set(access_token, forKey: "access_token")
                        UserDefaults.standard.set(self.firstName, forKey: "first_name")
                        UserDefaults.standard.set(self.lastName, forKey: "last_name")
                        UserDefaults.standard.set(self.image, forKey: "image")
                        
                        UserDefaults.standard.set(self.email, forKey: "email")
                        UserDefaults.standard.set(true, forKey: "isLoggedIn")
                        UserDefaults.standard.set(userid, forKey: "userid")
                        UserDefaults.standard.set(true, forKey: "FacebookUser")
                        self.updateDeviceToken()
                        
                        self.getAppSettings()
                        
                        //                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
                        //                            self.present(vc, animated: true, completion: nil)
                    }
                }
            }
            else{
                SwiftSpinner.hide()
                print(response.error.debugDescription)
                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
            }
        }
    }
    @IBAction func login(_ sender: Any) {
        
        let isValid = validateForm()
        if(isValid)
        {
            let params: Parameters = [
                "email": UsernameFld.text!,
                "user_type": "User",
                "password": passwordFld.text!
            ]
            SwiftSpinner.show("Logging in...")
            let url = "\(Constants.baseURL)/api/userlogin"
            Alamofire.request(url,method: .post,parameters:params).responseJSON { response in
                
                if(response.result.isSuccess)
                {
                    SwiftSpinner.hide()
                    if let json = response.result.value {
                        print("LOGIN JSON: \(json)") // serialized json response
                        let jsonResponse = JSON(json)
                        if(jsonResponse["error"].stringValue == "true")
                        {
                            let errorMessage = jsonResponse["error_message"].stringValue
                            self.showAlert(title: "Login Failed",msg: errorMessage)
                        }
                        else{
                            let access_token = "Bearer ".appending(jsonResponse["access_token"].stringValue)
                            let first_name = jsonResponse["first_name"].stringValue
                            
                            let last_name = jsonResponse["last_name"].stringValue
                            let image = jsonResponse["image"].stringValue
                            let mobile = jsonResponse["mobile"].stringValue
                            let email = jsonResponse["email"].stringValue
                            let userid = jsonResponse["user_id"].stringValue
                            
                            UserDefaults.standard.set(access_token, forKey: "access_token")
                            UserDefaults.standard.set(first_name, forKey: "first_name")
                            UserDefaults.standard.set(last_name, forKey: "last_name")
                            UserDefaults.standard.set(image, forKey: "image")
                            UserDefaults.standard.set(mobile, forKey: "mobile")
                            UserDefaults.standard.set(email, forKey: "email")
                            UserDefaults.standard.set(userid, forKey: "userid")
                            UserDefaults.standard.set(true, forKey: "isLoggedIn")
                            UserDefaults.standard.set(false, forKey: "FacebookUser")
                            self.updateDeviceToken()
                            
                           self.getAppSettings()
                            
//                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
//                            self.present(vc, animated: true, completion: nil)
                        }
                    }
                }
                else{
                    SwiftSpinner.hide()
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                }
                
            }
        }
        
       
    }
   
    func getAppSettings(){
        var headers : HTTPHeaders!
        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
        {
            headers = [
                "Authorization": accesstoken,
                "Accept": "application/json"
            ]
        }
        else
        {
            headers = [
                "Authorization": "",
                "Accept": "application/json"
            ]
        }
        
        let url = "\(Constants.baseURL)/api/appsettings"
        Alamofire.request(url,method: .get, headers:headers).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                if let json = response.result.value {
                    print("APP SETTINGS JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    if(jsonResponse["error"].stringValue == "true" )
                    {
                        self.showAlert(title: "Oops", msg: jsonResponse["error_message"].stringValue)
                    }
                    else if(jsonResponse["error"].stringValue == "Unauthenticated")
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                        Constants.locations = jsonResponse["location"].arrayValue
                        Constants.timeSlots = jsonResponse["timeslots"].arrayValue
                        
                        let statusArray = jsonResponse["status"].arrayValue;
                        
                        let isLoggedIn = UserDefaults.standard.bool(forKey: "isLoggedIn")
                        if(isLoggedIn)
                        {
                            self.updateDeviceToken()
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
                            MainViewController.status = statusArray
                            self.present(vc, animated: true, completion: nil)
                        }
                        else{
                            let isLoggedInSkipped = UserDefaults.standard.bool(forKey: "isLoggedInSkipped")
                            if(isLoggedInSkipped)
                            {
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
                                MainViewController.status = statusArray
                                self.present(vc, animated: true, completion: nil)
                            }
                            else{
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "OnboardViewController") as! OnboardViewController
                                self.present(vc, animated: true, completion: nil)
                            }
                        }
                    }
                }
            }
            else{
                print(response.error!.localizedDescription)
                //                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
                
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    func updateDeviceToken(){
        var headers : HTTPHeaders!
        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
        {
            headers = [
                "Authorization": accesstoken,
                "Accept": "application/json"
            ]
        }
        else
        {
            headers = [
                "Authorization": "",
                "Accept": "application/json"
            ]
        }

        let deviceToken = InstanceID.instanceID().token()
//        print(deviceToken)
        let params: Parameters = [
            "fcm_token": deviceToken!,
            "os":"iOS"
        ]
        let url = "\(Constants.baseURL)/api/updatedevicetoken"
        Alamofire.request(url,method: .post,parameters:params, headers:headers).responseJSON { response in
            
            print(response.description)
            
        }

    }
    
    @IBAction func facebookLogin(_ sender: Any){
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                // if user cancel the login
                if (result?.isCancelled)!{
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email"))
                {
                    self.getFBUserData()
                }
            }
        }
    }
    
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    //everything works print the user data
                    let jsonResponse = JSON(result!)
                    print(jsonResponse)
                    self.firstName = jsonResponse["first_name"].stringValue
                    self.lastName = jsonResponse["last_name"].stringValue
                    self.email = jsonResponse["email"].stringValue
                    self.image = jsonResponse["picture"]["data"]["url"].stringValue
                    let accessToken = FBSDKAccessToken.current().tokenString
                    self.socialLogin(socialType: "facebook", socialToken: accessToken)
                }
            })
        }
    }
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        // ...
        
        
            
            
            if error == nil
            {
                firstName = user.profile.givenName
                lastName = user.profile.familyName
                email = user.profile.email
                //            image = user.profile
                if user.profile.hasImage {
                    image = user.profile.imageURL(withDimension: 100).absoluteString
                }
                else {
                    image = " "
                }
                let token = user.authentication.accessToken
                self.socialLogin(socialType: "google", socialToken: token)
                
            }
        
        
        //
        //        guard let authentication = user.authentication else { return }
        //        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
        //                                                       accessToken: authentication.accessToken)
        
        
        // ...
    }
    
    @IBAction func googleLogin(_ sender: Any) {
        GIDSignIn.sharedInstance().delegate=self
        GIDSignIn.sharedInstance().uiDelegate=self
        GIDSignIn.sharedInstance().signIn()
    }
    
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func goToSignUp(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func goToForgotPassword(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.present(vc, animated: true, completion: nil)
    }
    
}
