//
//  MainViewController.swift
//  Aclena
//
//  Created by Karthik Sakthivel on 13/10/17.
//  Copyright © 2017 Uberdoo. All rights reserved.
//

import UIKit
import SwiftyJSON
import UserNotifications

class MainViewController: UITabBarController, UITabBarControllerDelegate,UNUserNotificationCenterDelegate {

    static var changePage = true
    static var goToBookings = false
    static var reloadPage = true
    static var status : [JSON]! = []
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate  = self
        }
        
        self.delegate = self;

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
       
        UITabBar.appearance().shadowImage = UIImage()
        UITabBar.appearance().backgroundImage = UIImage()

        if(MainViewController.changePage)
        {
            if(!MainViewController.goToBookings)
            {
                self.selectedIndex = 1
            }
            else{
                MainViewController.goToBookings = false
            }
        }        
        
    }

    override func viewDidAppear(_ animated: Bool) {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler(UNNotificationPresentationOptions(rawValue: UNNotificationPresentationOptions.RawValue(UInt8(UNNotificationPresentationOptions.alert.rawValue) | UInt8(UNNotificationPresentationOptions.sound.rawValue))))
    }
    
   
}
