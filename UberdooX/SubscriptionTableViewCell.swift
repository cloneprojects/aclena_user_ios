//
//  SubscriptionTableViewCell.swift
//  Aclena
//
//  Created by Karthik Sakthivel on 18/02/18.
//  Copyright © 2018 Uberdoo. All rights reserved.
//

import UIKit

class SubscriptionTableViewCell: UITableViewCell {

    @IBOutlet weak var serviceTime: UILabel!
    @IBOutlet weak var serviceDate: UILabel!
    @IBOutlet weak var subCategoryName: UILabel!
    @IBOutlet weak var recurringType: UILabel!
    @IBOutlet weak var subCategoryImage: UIImageView!
    
    @IBOutlet weak var providerName: UILabel!
    
    @IBOutlet weak var bookingStatusImageView: UIImageView!
    @IBOutlet weak var bookingStatusLbl: UILabel!
    @IBOutlet weak var cancel: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
