//
//  ReviewViewController.swift
//  Aclena
//
//  Created by Karthik Sakthivel on 29/10/17.
//  Copyright © 2017 Uberdoo. All rights reserved.
//

import UIKit
import Cosmos
import SwiftyJSON
import Alamofire
import SwiftSpinner

class ReviewViewController: UIViewController {
    
    var bookingDetails : [String:JSON]!
    @IBOutlet weak var reviewFld: UITextField!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var topView: UIView!
    var hasAlreadyMoved = false
    override func viewDidLoad() {
        super.viewDidLoad()

        print(self.bookingDetails)
        self.topView.frame.origin.y = self.topView.frame.origin.y + 130
        self.bottomView.frame.origin.y = self.bottomView.frame.origin.y + 270
        ratingView.didFinishTouchingCosmos = {
            rating in
            
            if(!self.hasAlreadyMoved)
            {
                UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.showHideTransitionViews, animations: {

                self.hasAlreadyMoved = true
                self.topView.frame.origin.y = self.topView.frame.origin.y - 130
                self.bottomView.frame.origin.y = self.bottomView.frame.origin.y - 270
                    
                }) { (Void) in
                    
                }
            }
            print(rating)
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func sendReview(_ sender: Any) {
        var headers : HTTPHeaders!
        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
        {
            headers = [
                "Authorization": accesstoken,
                "Accept": "application/json"
            ]
        }
        else
        {
            headers = [
                "Authorization": "",
                "Accept": "application/json"
            ]
        }
        
        let bookingId = self.bookingDetails["booking_id"]!.stringValue//provider_id
        let providerId = self.bookingDetails["provider_id"]!.stringValue
        let rating = String(ratingView.rating)
        let feedback = reviewFld.text
        
        let params: Parameters = [
            "id": providerId,
            "rating": rating ,
            "booking_id":bookingId,
            "feedback":feedback!
        ]
        
        
        SwiftSpinner.show("Thanks for your review.")
        let url = "\(Constants.baseURL)/api/review"
        Alamofire.request(url,method: .post, parameters:params, headers:headers).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                SwiftSpinner.hide()
                if let json = response.result.value {
                    print("REVIEW JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    
                    
                    if(jsonResponse["error"].stringValue == "true")
                    {
                        let errorMessage = jsonResponse["error_message"].stringValue
                        self.showAlert(title: "Failed",msg: errorMessage)
                    }
                    else{
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RecurringViewController") as! RecurringViewController
                        vc.bookingDetails = self.bookingDetails
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }
            else{
                SwiftSpinner.hide()
                print(response.error.debugDescription)
                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
            }
        }
    }
    
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
