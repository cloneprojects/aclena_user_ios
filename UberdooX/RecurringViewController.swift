//
//  RecurringViewController.swift
//  Aclena
//
//  Created by Karthik Sakthivel on 19/02/18.
//  Copyright © 2018 Uberdoo. All rights reserved.
//

import UIKit
import Cosmos
import SwiftyJSON
import SwiftSpinner
import Alamofire

class RecurringViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var recurralTypePicker: UIPickerView!
    @IBOutlet weak var providerNameLbl: UILabel!
    @IBOutlet weak var serviceNameLbl: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var recurringTypeLbl: UILabel!
    @IBOutlet weak var providerSchedulesCollectionView: UICollectionView!
    
    var recurralTypes = ["Weekly", "BiWeekly", "Monthly"]
    
    @IBOutlet weak var saveButton: UIButton!
    var selectedTimeSlots = [Bool]()
    var validTimeSlots : [JSON] = []
    var bookingDetails : [String:JSON]!
    var selectedTimeSlotId : String!
    var selectedTimeSlotName : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLbl.text = "Need same service again from \(self.bookingDetails["providername"]!.stringValue)"
        
        selectedTimeSlotId = ""
        print(bookingDetails)
        providerNameLbl.text = self.bookingDetails["providername"]!.stringValue
        serviceNameLbl.text = self.bookingDetails["sub_category_name"]!.stringValue
        if let rating = self.bookingDetails["rating"]?.double{
            ratingView.rating = rating
        }
        recurringTypeLbl.text = recurralTypes[0]
        recurralTypePicker.delegate = self
        recurralTypePicker.dataSource = self
        
        for i in 0 ... Constants.timeSlots.count-1
        {
            validTimeSlots.append(Constants.timeSlots[i])
            selectedTimeSlots.append(false)
        }
        providerSchedulesCollectionView.delegate = self
        providerSchedulesCollectionView.dataSource = self

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
//        getProviderTimeSlots()
//        getProviderTimeSchedules()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TimeSlotsCollectionViewCell", for: indexPath) as! TimeSlotsCollectionViewCell
        let title = validTimeSlots[indexPath.row]["timing"].stringValue
        cell.timeSlotBtn.setTitle(title, for: UIControlState.normal)
        print(indexPath.row)
        cell.timeSlotBtn.isUserInteractionEnabled = false
        if(selectedTimeSlots[indexPath.row])
        {
            cell.timeSlotBtn.setTitleColor(UIColor.white, for: UIControlState.normal)
            cell.timeSlotBtn.backgroundColor = UIColor.init(red: 107/255, green: 127/255, blue: 252/255, alpha: 1)
        }
        else{
            cell.timeSlotBtn.backgroundColor = UIColor.init(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
            cell.timeSlotBtn.setTitleColor(UIColor.init(red: 52/255, green: 53/255, blue: 72/255, alpha: 1), for: UIControlState.normal)
        }
        
        return cell
    
    }
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return validTimeSlots.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedTimeSlots.removeAll()
        for i in 0 ... validTimeSlots.count-1 {
            if(i == indexPath.row)
            {
                selectedTimeSlotId = validTimeSlots[indexPath.row]["id"].stringValue
                selectedTimeSlotName = validTimeSlots[i]["timing"].stringValue
                self.selectedTimeSlots.append(true)
            }
            else{
                self.selectedTimeSlots.append(false)
            }
        }
        print(self.selectedTimeSlots)
        self.providerSchedulesCollectionView.reloadData()
    }
    
    @IBAction func skipRecurringService(_ sender: Any) {
        var headers : HTTPHeaders!
        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
        {
            headers = [
                "Authorization": accesstoken,
                "Accept": "application/json"
            ]
        }
        else
        {
            headers = [
                "Authorization": "",
                "Accept": "application/json"
            ]
        }
        let bookingId = self.bookingDetails["booking_id"]!.stringValue
        let params: Parameters = [
            "booking_id": bookingId
        ]
        let url = "\(Constants.baseURL)/api/reject_recurral"
        Alamofire.request(url,method: .post,parameters:params, headers:headers).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                SwiftSpinner.hide()
                if let json = response.result.value {
                    print("SKIP RECURRAL: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    if(jsonResponse["error"].stringValue == "true" )
                    {
                        self.showAlert(title: "Oops", msg: jsonResponse["error_message"].stringValue)
                    }
                    else if(jsonResponse["error"].stringValue == "Unauthenticated")
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SplashViewController") as! SplashViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }
            else{
                SwiftSpinner.hide()
                print(response.error!.localizedDescription)
                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                
            }
            
        }
        
    }
    
    @IBAction func changeRecurralType(_ sender: Any) {
        recurralTypePicker.isHidden = false
    }
    @IBAction func saveRecurringService(_ sender: Any) {
        
        if(selectedTimeSlotId == "")
        {
            self.showAlert(title: "Validation Failed", msg: "Please select one of the time slots")
        }
        else{
            var headers : HTTPHeaders!
            if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
            {
                headers = [
                    "Authorization": accesstoken,
                    "Accept": "application/json"
                ]
            }
            else
            {
                headers = [
                    "Authorization": "",
                    "Accept": "application/json"
                ]
            }
            let bookingId = self.bookingDetails["booking_id"]!.stringValue
            let params: Parameters = [
                "recurrent_type": "Weekly",
                "id" : bookingId,
                "provider_schedules_id" : selectedTimeSlotId
            ]
            let url = "\(Constants.baseURL)/api/book_recurrent"
            Alamofire.request(url,method: .post,parameters:params, headers:headers).responseJSON { response in
                
                if(response.result.isSuccess)
                {
                    SwiftSpinner.hide()
                    if let json = response.result.value {
                        print("PROVIDER TIME SLOTS JSON: \(json)") // serialized json response
                        let jsonResponse = JSON(json)
                        if(jsonResponse["error"].stringValue == "true" )
                        {
                            self.showAlert(title: "Oops", msg: jsonResponse["error_message"].stringValue)
                        }
                        else if(jsonResponse["error"].stringValue == "Unauthenticated")
                        {
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
                            self.present(vc, animated: true, completion: nil)
                        }
                        else{
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SplashViewController") as! SplashViewController
                            self.present(vc, animated: true, completion: nil)
                        }
                    }
                }
                else{
                    SwiftSpinner.hide()
                    print(response.error!.localizedDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    
                }
                
            }
        }
        func getProviderTimeSchedules(){
            var headers : HTTPHeaders!
            if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
            {
                headers = [
                    "Authorization": accesstoken,
                    "Accept": "application/json"
                ]
            }
            else
            {
                headers = [
                    "Authorization": "",
                    "Accept": "application/json"
                ]
            }
            print(UserDefaults.standard.string(forKey: "access_token") as String!)
            SwiftSpinner.show("Fetching Provider Schedules...")
            let url = "\(Constants.baseURL)/api/view_schedules"
            Alamofire.request(url,method: .get,  headers:headers).responseJSON { response in
                
                if(response.result.isSuccess)
                {
                    SwiftSpinner.hide()
                    if let json = response.result.value {
                        print("PROVIDER SCHEDULES JSON: \(json)") // serialized json response
                        let jsonResponse = JSON(json)
                        if(jsonResponse["error"].stringValue == "true" )
                        {
                            self.showAlert(title: "Oops", msg: jsonResponse["error_message"].stringValue)
                        }
                        else if(jsonResponse["error"].stringValue == "Unauthenticated")
                        {
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
                            self.present(vc, animated: true, completion: nil)
                        }
                        else{
                            
                        }
                    }
                }
                else{
                    SwiftSpinner.hide()
                    print(response.error!.localizedDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    
                }
            }

        }
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 3
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return recurralTypes[row]
    }
    
    func pickerView(_: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        recurringTypeLbl.text = recurralTypes[row]
        recurralTypePicker.isHidden = true;
    }
    
//    func getProviderTimeSlots(){
//        var headers : HTTPHeaders!
//        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
//        {
//            headers = [
//                "Authorization": accesstoken,
//                "Accept": "application/json"
//            ]
//        }
//        else
//        {
//            headers = [
//                "Authorization": "",
//                "Accept": "application/json"
//            ]
//        }
//
//        let params: Parameters = [
//            "provider_id": "4"
//        ]
//        let url = "\(Constants.baseURL)/api/list_provider_timeslots"
//        Alamofire.request(url,method: .post,parameters:params, headers:headers).responseJSON { response in
//
//            if(response.result.isSuccess)
//            {
//                SwiftSpinner.hide()
//                if let json = response.result.value {
//                    print("PROVIDER TIME SLOTS JSON: \(json)") // serialized json response
//                    let jsonResponse = JSON(json)
//                    if(jsonResponse["error"].stringValue == "true" )
//                    {
//                        self.showAlert(title: "Oops", msg: jsonResponse["error_message"].stringValue)
//                    }
//                    else if(jsonResponse["error"].stringValue == "Unauthenticated")
//                    {
//                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
//                        self.present(vc, animated: true, completion: nil)
//                    }
//                    else{
//
//                    }
//                }
//            }
//            else{
//                SwiftSpinner.hide()
//                print(response.error!.localizedDescription)
//                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
//
//            }
//
//        }
//
//    }
}
