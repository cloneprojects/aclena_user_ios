//
//  EditProfileViewController.swift
//  Aclena
//
//  Created by Karthik Sakthivel on 03/02/18.
//  Copyright © 2018 Uberdoo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SwiftSpinner
import Nuke

protocol updateImageDelegate
{
    func updateImage()
}

class EditProfileViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    var imageName : String!
    @IBOutlet weak var phoneNumberTxtFld: UITextField!
    @IBOutlet weak var lastNameTxtFld: UITextField!
    @IBOutlet weak var firstNameTxtFld: UITextField!
    @IBOutlet weak var profilePicture: UIImageView!
    var updateDelegate: updateImageDelegate?
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.getProfile()
    }

    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        if let image = UserDefaults.standard.string(forKey: "image") as String!
        {
            if let imageUrl = URL.init(string: image) as URL!
            {
                Nuke.loadImage(with: imageUrl, into: self.profilePicture)
            }
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func getProfile()
    {
        var headers : HTTPHeaders!
        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
        {
            headers = [
                "Authorization": accesstoken,
                "Accept": "application/json"
            ]
        }
        else
        {
            headers = [
                "Authorization": "",
                "Accept": "application/json"
            ]
        }
        
        SwiftSpinner.show("Fetching Profile Details...")
        let url = "\(Constants.baseURL)/api/viewprofile"
        Alamofire.request(url,method: .get, headers:headers).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                SwiftSpinner.hide()
                if let json = response.result.value {
                    print("VIEW PROFILE JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    if(jsonResponse["error"].stringValue == "Unauthenticated" || jsonResponse["error"].stringValue == "true")
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                        self.firstNameTxtFld.text = jsonResponse["user_details"]["first_name"].stringValue
                        self.lastNameTxtFld.text = jsonResponse["user_details"]["last_name"].stringValue
                        self.phoneNumberTxtFld.text = jsonResponse["user_details"]["mobile"].stringValue
                        if let image = jsonResponse["user_details"]["image"].string{
                            self.imageName = image
                            if let imageUrl = URL.init(string: image) as URL!{
                                Nuke.loadImage(with: imageUrl, into: self.profilePicture)
                            }
                        }
                    }
                }
            }
            else{
                SwiftSpinner.hide()
                print(response.error.debugDescription)
                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                
            }
        }
    }
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func profilePictureClicked(_ sender: Any) {
        let picker = UIImagePickerController()
        picker.delegate = self
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: {
            action in
            
            picker.sourceType = .camera
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: {
            action in
            picker.sourceType = .photoLibrary
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.profilePicture.image = image
        picker.dismiss(animated: true)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func uploadImage(){
        let url = "\(Constants.adminBaseURL)/imageupload"
        _ = try! URLRequest(url: url, method: .post)
        let img = self.profilePicture.image
        let imagedata = UIImageJPEGRepresentation(img!, 0.6)
        
        
        
        SwiftSpinner.show("Uploading Image")
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if let data = imagedata{
                multipartFormData.append(data, withName: "file", fileName: "image.png", mimeType: "image/png")
            }
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: nil, encodingCompletion: { (result) in
            
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print(response)
                    let jsonResponse = JSON(response.result.value)
                    //                    print(jsonResponse)
                    self.imageName = jsonResponse["image"].stringValue
                    print("Succesfully uploaded")
                    self.setProfile()
                    if let err = response.error{
                        self.showAlert(title: "Oops", msg: "Something went wrong")
                        print(err)
                        return
                    }
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
            }
        })
        
    }
    
    @IBAction func savePressed(_ sender: Any) {
        if(firstNameTxtFld.text != "")
        {
            if(lastNameTxtFld.text != "")
            {
                if(phoneNumberTxtFld.text != "")
                {
                    self.uploadImage()
                }
                else{
                    self.showAlert(title: "Validation Failed", msg: "Invalid Phone Number")
                }
                
            }
            else{
                self.showAlert(title: "Validation Failed", msg: "Invalid Last Name")
            }
        }
        else{
            self.showAlert(title: "Validation Failed", msg: "Invalid First Name")
        }
        
    }
    
    func setProfile()
    {
        var headers : HTTPHeaders!
        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
        {
            headers = [
                "Authorization": accesstoken,
                "Accept": "application/json"
            ]
        }
        else
        {
            headers = [
                "Authorization": "",
                "Accept": "application/json"
            ]
        }
        
        let params: Parameters = [
            "first_name": firstNameTxtFld.text!,
            "last_name": lastNameTxtFld.text!,
            "mobile": phoneNumberTxtFld.text!,
            "image": self.imageName
        ]

        
        SwiftSpinner.show("Updating Profile Details...")
        let url = "\(Constants.baseURL)/api/updateprofile"
        Alamofire.request(url,method: .post, parameters:params, headers:headers).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                SwiftSpinner.hide()
                if let json = response.result.value {
                    print("EDIT PROFILE JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    if(jsonResponse["error"].stringValue == "Unauthenticated" || jsonResponse["error"].stringValue == "true")
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                        UserDefaults.standard.set(self.imageName, forKey: "image")
                        UserDefaults.standard.set(self.firstNameTxtFld.text, forKey: "first_name")
                        UserDefaults.standard.set(self.lastNameTxtFld.text, forKey: "last_name")
                        self.updateDelegate?.updateImage()
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
            else{
                SwiftSpinner.hide()
                print(response.error.debugDescription)
                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                
            }
        }

    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
