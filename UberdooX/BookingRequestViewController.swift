//
//  BookingRequestViewController.swift
//  Aclena
//
//  Created by Karthik Sakthivel on 27/10/17.
//  Copyright © 2017 Uberdoo. All rights reserved.
//

import UIKit

class BookingRequestViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func goToHomePage(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        self.present(vc, animated: true, completion: nil)
    }
    
}
