//
//  SubscriptionViewController.swift
//  Aclena
//
//  Created by Karthik Sakthivel on 18/02/18.
//  Copyright © 2018 Uberdoo. All rights reserved.
//

import UIKit
import SwiftyJSON
import SwiftSpinner
import Alamofire
import Nuke

class SubscriptionViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var refreshControl: UIRefreshControl!

    @IBOutlet weak var tableView: UITableView!
    var bookings : [JSON] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
       
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action:  #selector(BookingsViewController.refresh(sender:)), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        
        getMySubscriptions()
    }
    
    @objc func refresh(sender: UIRefreshControl) {
        getMySubscriptions()
    }
    
    @objc func cancelPressed(sender:UIButton!) {
        print("Cancel Clicked \(sender.tag)")
        
        let alert = UIAlertController(title: "Confirm", message: "Are you sure you want to cancel this subscription?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: {
            (alert: UIAlertAction!) in
            print ("YES. CANCEL BOOKING ID :\(self.bookings[sender.tag]["booking_order_id"].stringValue)")
            let bookingId = self.bookings[sender.tag]["id"].stringValue
            self.cancelBooking(bookingId: bookingId)
        }))
        
        
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: {
            (alert: UIAlertAction!) in
        }))
        
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func getMySubscriptions(){
        var headers : HTTPHeaders!
        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
        {
            headers = [
                "Authorization": accesstoken,
                "Accept": "application/json"
            ]
        }
        else
        {
            headers = [
                "Authorization": "",
                "Accept": "application/json"
            ]
        }
        
        SwiftSpinner.show("Fetching your Subscriptions...")
        let url = "\(Constants.baseURL)/api/list_provider_schedules"
        Alamofire.request(url,method: .get, headers:headers).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                self.refreshControl.endRefreshing()
                SwiftSpinner.hide()
                if let json = response.result.value {
                    print("BOOKINGS JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    if(jsonResponse["error"].stringValue == "true" )
                    {
                        self.showAlert(title: "Oops", msg: jsonResponse["error_message"].stringValue)
                    }
                    else if(jsonResponse["error"].stringValue == "Unauthenticated")
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                        self.bookings = jsonResponse["all_scheduled_bookings"].arrayValue
                        self.tableView.dataSource = self
                        self.tableView.delegate = self
                        self.tableView.reloadData()
                    }
                }
            }
            else{
                SwiftSpinner.hide()
                print(response.error.debugDescription)
                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                
            }
        }
    }
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func cancelBooking(bookingId: String)
    {
        var headers : HTTPHeaders!
        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
        {
            headers = [
                "Authorization": accesstoken,
                "Accept": "application/json"
            ]
        }
        else
        {
            headers = [
                "Authorization": "",
                "Accept": "application/json"
            ]
        }
        
        let params: Parameters = [
            "id": bookingId
        ]
        SwiftSpinner.show("Cancelling your Subscription...")
        let url = "\(Constants.baseURL)/api/cancel_schedule"
        Alamofire.request(url,method: .post, parameters:params, headers:headers).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                self.refreshControl.endRefreshing()
                SwiftSpinner.hide()
                if let json = response.result.value {
                    print("CANCEL JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    if(jsonResponse["error"].stringValue == "true" )
                    {
                        self.showAlert(title: "Oops", msg: jsonResponse["error_message"].stringValue)
                    }
                    else if(jsonResponse["error"].stringValue == "Unauthenticated")
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                        self.getMySubscriptions()
                    }
                }
            }
            else{
                SwiftSpinner.hide()
                print(response.error.debugDescription)
                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubscriptionTableViewCell", for: indexPath) as! SubscriptionTableViewCell
        cell.subCategoryName.text = self.bookings[indexPath.row]["sub_category_name"].stringValue
        cell.serviceDate.text = self.bookings[indexPath.row]["booking_date"].stringValue
        cell.serviceTime.text = self.bookings[indexPath.row]["timing"].stringValue
        if let imageURL = URL.init(string:self.bookings[indexPath.row]["image"].stringValue)
        {
            Nuke.loadImage(with: imageURL, into: cell.subCategoryImage)
        }
        cell.recurringType.text = self.bookings[indexPath.row]["recurrent_type"].stringValue
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.providerName.text = self.bookings[indexPath.row]["name"].stringValue
        cell.cancel.tag = indexPath.row
        cell.cancel.addTarget(self, action: #selector(self.cancelPressed(sender:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BookingDetailViewController") as! BookingDetailViewController
//        MainViewController.changePage = false
//        vc.bookingDetails = self.bookings[indexPath.row].dictionary
//        self.present(vc, animated: true, completion: nil)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 175
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.bookings.count
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func backPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

}
