//
//  InvoiceViewController.swift
//  SwyftX
//
//  Created by Karthik Sakthivel on 29/10/17.
//  Copyright © 2017 Swyft. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SwiftSpinner
import UserNotifications
import Stripe

class InvoiceViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UNUserNotificationCenterDelegate,STPPaymentContextDelegate {
    
    
    @IBOutlet weak var addCardButton: UIButton!
    @IBOutlet weak var changeButton: UIButton!
    @IBOutlet weak var selectedCardDetailsLbl: UILabel!
    @IBOutlet weak var selectedCardLbl: UILabel!
    private let customerContext: STPCustomerContext
    private let paymentContext: STPPaymentContext
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var taxNameLbl: UILabel!
    @IBOutlet weak var taxLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var workingHoursLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var bookingIdLbl: UILabel!
    @IBOutlet weak var billingNameLbl: UILabel!
    @IBOutlet weak var providerNameLbl: UILabel!
    @IBOutlet weak var paymentTypes: UICollectionView!
    var bookingDetails : [String:JSON]!
    var paymentTypeNames: [String] = ["Cash","Card"]
    var paymentTypeImageSelected: [String] = ["cash_select","card_select"]
    var paymentTypeImageUnSelected: [String] = ["cash_unselect","card_unselect"]
    var selectedPaymentTypes: [Bool] = [true,false]
    
    private var price = "0"
    //{        didSet {
    //            // Forward value to payment context
    //            paymentContext.pa = Int(price)
    //        }
    //    }
    
    @IBOutlet weak var confirmButton: UIButton!
    
    required init?(coder aDecoder: NSCoder)
    {
        let config = STPPaymentConfiguration.shared()
        config.publishableKey = "pk_live_phmnh4oRoomaGGWNYjix2qZD"
        config.companyName = "self.companyName"
        config.requiredBillingAddressFields = STPBillingAddressFields.none
        config.requiredShippingAddressFields = .none
        config.shippingType = STPShippingType.delivery
        
        customerContext = STPCustomerContext(keyProvider: MyAPIClient.sharedClient)
        paymentContext = STPPaymentContext(customerContext: customerContext,configuration:config,theme:STPTheme.default())
        
        super.init(coder: aDecoder)
        
        
        // Create card sources instead of card tokens
        
        paymentContext.delegate = self
        paymentContext.hostViewController = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.paymentTypes.delegate = self
        self.paymentTypes.dataSource = self
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
        }
        
        
        selectedCardLbl.isHidden = true
        selectedCardDetailsLbl.isHidden = true
        changeButton.isHidden = true
        
     
          let total  = self.bookingDetails["total_cost"]!.double
        let tot = String(format: "%.2f", (total! / 100.1) * 100)
        self.totalLbl.text = "£ \(String(tot))"
        self.price = self.bookingDetails["total_cost"]!.stringValue
        self.taxNameLbl.text = "\(String(describing: self.bookingDetails["tax_name"]!.stringValue)) (\(self.bookingDetails["gst_percent"]!.stringValue)%)"
   
          let tax  =  self.bookingDetails["gst_cost"]!.double
        let VAT = String(format: "%.2f", (tax! / 100.1) * 100)
        taxLbl.text = "£ \(String(VAT))"
        let price = self.bookingDetails["cost"]!.double
        let amount = String(format: "%.2f", (price! / 100.1) * 100)
        self.priceLbl.text = "£ \(String(amount))"
        let workhrs = minutesToHoursMinutes(minutes: self.bookingDetails["worked_mins"]!.intValue)
        if(workhrs.hours > 0){
            self.workingHoursLbl.text = "\(workhrs.hours) Hr & \(workhrs.leftMinutes) mins"
        }
        else{
            self.workingHoursLbl.text = "\(workhrs.leftMinutes) mins"
        }
        
        self.timeLbl.text = self.bookingDetails["timing"]?.stringValue
        self.dateLbl.text = self.bookingDetails["booking_date"]?.stringValue
        self.bookingIdLbl.text = self.bookingDetails["booking_order_id"]?.stringValue
        self.billingNameLbl.text = self.bookingDetails["username"]?.stringValue
        self.providerNameLbl.text = self.bookingDetails["providername"]?.stringValue
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func payByCash(){
        var headers : HTTPHeaders!
        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
        {
            headers = [
                "Authorization": accesstoken,
                "Accept": "application/json"
            ]
        }
        else
        {
            headers = [
                "Authorization": "",
                "Accept": "application/json"
            ]
        }
        
        let bookingId = self.bookingDetails["booking_id"]!.stringValue
        let params: Parameters = [
            "id": bookingId,
            "method": "cash"
        ]
        
        
        SwiftSpinner.show("Making your payment...")
        let url = "\(Constants.baseURL)/api/pay"
        Alamofire.request(url,method: .post, parameters:params, headers:headers).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                SwiftSpinner.hide()
                if let json = response.result.value {
                    print("CHANGE PASSWORD JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    
                    
                    if(jsonResponse["error"].stringValue == "true")
                    {
                        let errorMessage = jsonResponse["error_message"].stringValue
                        self.showAlert(title: "Failed",msg: errorMessage)
                    }
                    else{
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WaitingForPaymentConfirmationViewController") as! WaitingForPaymentConfirmationViewController
                        vc.bookingDetails = self.bookingDetails
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }
            else{
                SwiftSpinner.hide()
                print(response.error.debugDescription)
                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
            }
        }
        
    }
    
    @IBAction func addANewCard(_ sender: Any) {
        self.paymentContext.presentPaymentMethodsViewController()
    }
    func payByCard()
    {
        self.paymentContext.requestPayment()
        
    }
    
    
    
    @IBAction func initiatePayment(_ sender: Any) {
        if(selectedPaymentTypes[0])
        {
            payByCash()
        }
        else
        {
            payByCard()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //paymentTypeCell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "paymentTypeCell", for: indexPath) as! PaymentTypeCollectionViewCell
        
        cell.typeName.text = self.paymentTypeNames[indexPath.row]
        if(selectedPaymentTypes[indexPath.row])
        {
            cell.typeImage.image = UIImage.init(named: self.paymentTypeImageSelected[indexPath.row])
            cell.typeName.textColor = UIColor.init(red: 107/255, green: 127/255, blue: 252/255, alpha: 1)
        }
        else{
            cell.typeImage.image = UIImage.init(named: self.paymentTypeImageUnSelected[indexPath.row])
            cell.typeName.textColor = UIColor.init(red: 29/255, green: 29/255, blue: 29/255, alpha: 1)
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedPaymentTypes.removeAll()
        for i in 0 ... self.paymentTypeNames.count {
            if(i == indexPath.row)
            {
                selectedPaymentTypes.append(true)
            }
            else{
                selectedPaymentTypes.append(false)
            }
        }
        if(selectedPaymentTypes[0])
        {
            selectedCardLbl.isHidden = true
            selectedCardDetailsLbl.isHidden = true
            changeButton.isHidden = true
            confirmButton.setTitle("PAY BY CASH", for: .normal)
        }
        else{
            selectedCardLbl.isHidden = false
            selectedCardDetailsLbl.isHidden = false
            changeButton.isHidden = false
            confirmButton.setTitle("PAY BY CARD", for: .normal)
            
            if let methods = paymentContext.paymentMethods
            {
                if(methods.count > 0)
                {
                    
                    self.selectedCardDetailsLbl.isHidden = false
                    self.changeButton.isHidden = false
                    self.addCardButton.isHidden = true
                }
                else
                {
                    self.selectedCardDetailsLbl.isHidden = true
                    self.changeButton.isHidden = true
                    self.addCardButton.isHidden = false
                }
            }
            
        }
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return paymentTypeNames.count
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        getAppSettings()
        //        completionHandler(UNNotificationPresentationOptions(rawValue: UNNotificationPresentationOptions.RawValue(UInt8(UNNotificationPresentationOptions.alert.rawValue) | UInt8(UNNotificationPresentationOptions.sound.rawValue))))
        
    }
    
    func getAppSettings(){
        var headers : HTTPHeaders!
        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
        {
            headers = [
                "Authorization": accesstoken,
                "Accept": "application/json"
            ]
        }
        else
        {
            headers = [
                "Authorization": "",
                "Accept": "application/json"
            ]
        }
        
        let url = "\(Constants.baseURL)/api/appsettings"
        Alamofire.request(url,method: .get, headers:headers).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                if let json = response.result.value {
                    print("APP SETTINGS JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    if(jsonResponse["error"].stringValue == "true" )
                    {
                    }
                    else if(jsonResponse["error"].stringValue == "Unauthenticated")
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                        Constants.locations = jsonResponse["location"].arrayValue
                        Constants.timeSlots = jsonResponse["timeslots"].arrayValue
                        
                        let statusArray = jsonResponse["status"].arrayValue;
                        if(statusArray.count > 0){
                            let statusDict = statusArray[0].dictionary
                            let currentStatus = statusDict!["status"]?.stringValue
                            if(currentStatus == "Completedjob")
                            {
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "InvoiceViewController") as! InvoiceViewController
                                vc.bookingDetails = statusDict
                                vc.modalPresentationStyle = .overCurrentContext
                                self.present(vc, animated: true, completion: nil)
                            }
                            else if(currentStatus == "Waitingforpaymentconfirmation"){
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "WaitingForPaymentConfirmationViewController") as! WaitingForPaymentConfirmationViewController
                                vc.bookingDetails = statusDict
                                self.present(vc, animated: true, completion: nil)
                            }
                            else if(currentStatus == "Reviewpending"){
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReviewViewController") as!     ReviewViewController
                                vc.bookingDetails = statusDict
                                self.present(vc, animated: true, completion: nil)
                            }
                        }
                    }
                }
            }
            else{
                print(response.error!.localizedDescription)
                //                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
                
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    func minutesToHoursMinutes (minutes : Int) -> (hours : Int , leftMinutes : Int) {
        return (minutes / 60, (minutes % 60))
    }
    
    
    func paymentContext(_ paymentContext: STPPaymentContext,
                        didCreatePaymentResult paymentResult: STPPaymentResult,
                        completion: @escaping STPErrorBlock) {
        print(paymentResult)
        //
        var myAPIClient = MyAPIClient.init()
        let bookingId = self.bookingDetails["booking_id"]!.stringValue
        myAPIClient.completeCharge(paymentResult, amount: price, bookingId: bookingId, completion: { (error: Error?) in
            if let error = error {
                completion(error)
            } else {
                completion(nil)
            }
        })
    }
    
    @IBAction func changePaymentMethod(_ sender: Any) {
        self.paymentContext.presentPaymentMethodsViewController()
    }
    
    func paymentContextDidChange(_ paymentContext: STPPaymentContext) {
        
        if let stpPM = paymentContext.selectedPaymentMethod
        {
            print(stpPM)
            let stpCard = stpPM as! STPCard
            var cardBrand = "Card"
            if (stpCard.brand == STPCardBrand.visa)
            {
                cardBrand = "VISA"
            }
            else if (stpCard.brand == STPCardBrand.amex)
            {
                cardBrand = "AMEX"
            }
            else if (stpCard.brand == STPCardBrand.masterCard)
            {
                cardBrand = "MASTERCARD"
            }
            else if (stpCard.brand == STPCardBrand.discover)
            {
                cardBrand = "DISCOVER"
            }
            else if (stpCard.brand == STPCardBrand.JCB)
            {
                cardBrand = "JCB"
            }
            else if (stpCard.brand == STPCardBrand.dinersClub)
            {
                cardBrand = "DINERS CLUB"
            }
            self.selectedCardDetailsLbl.text = "\(cardBrand) Ending in \(stpCard.last4)"
        }
        
        if(selectedPaymentTypes[1])
        {
            if let methods = paymentContext.paymentMethods
            {
                if(methods.count > 0)
                {
                    
                    self.selectedCardDetailsLbl.isHidden = false
                    self.changeButton.isHidden = false
                    self.addCardButton.isHidden = true
                }
                else
                {
                    self.selectedCardDetailsLbl.isHidden = true
                    self.changeButton.isHidden = true
                    self.addCardButton.isHidden = false
                }
            }
        }
        
        
    }
    
    func paymentContext(_ paymentContext: STPPaymentContext,
                        didFinishWith    status: STPPaymentStatus,
                        error: Error?) {
        
        switch status {
        case .error:
            //            self.showError(error)
            print(error)
        case .success:
            print("Success")
            self.getAppSettings()
        case .userCancellation:
            return // Do nothing
        }
    }
    
    func paymentContext(_ paymentContext: STPPaymentContext,
                        didFailToLoadWithError error: Error) {
        self.navigationController?.popViewController(animated: true)
        // Show the error to your user, etc.
    }
    
    
}

